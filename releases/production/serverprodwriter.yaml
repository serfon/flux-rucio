---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: serverprodwriter
  namespace: rucio
  annotations:
    flux.weave.works/automated: "false"
spec:
  releaseName: serverprodwriter
  interval: 5m
  install:
    disableWait: true
  chart:
    spec:
      sourceRef:
        kind: HelmRepository
        name: rucio-charts
      chart: rucio-server
      version: 32.0.0
  valuesFrom:
    - kind: Secret
      name: db-secret-server-writer
      valuesKey: values.yaml
    - kind: Secret
      name: lb-secret
      valuesKey: values.yaml
    - kind: Secret
      name: tracer-secret
      valuesKey: values.yaml
    - kind: Secret
      name: nongrid-tracer-secret
      valuesKey: values.yaml
    - kind: Secret
      name: geoip-secret
      valuesKey: values.yaml
  values:
    replicaCount: 4

    useDeprecatedImplicitSecrets: false

    image:
      repository: rucio/rucio-server
      tag: release-32.2.0
      pullPolicy: IfNotPresent

    automaticRestart:
      enabled: 0

    service:
      type: LoadBalancer
      port: 80
      targetPort: 80
      protocol: TCP
      name: http
      allocateLoadBalancerNodePorts: false
      loadBalancerClass: purelb.io/purelb
      externalTrafficPolicy: Local

    monitoring:
      enabled: true
      labels:
        release: prometheus-operator
        prometheus: cluster-monitor
      serviceMonitor:
        relabelings:
          - targetLabel: metric_source
            replacement: rucio-k8s

    secretMounts:
      - secretFullName: rucio-x509up
        mountPath: /opt/proxy
      - secretFullName: cern-ca-bundle
        mountPath: /opt/certs/
      - secretFullName: serverprod-common-rucio-cfg
        mountPath: /opt/rucio/etc/conf.d/05_serverprod_common_rucio.cfg
        subPath: serverprod_common_rucio.cfg
      - secretFullName: gcsrucio
        mountPath: /opt/rucio/etc/
        subPaths:
          - gcs_rucio.json
      - secretFullName: gcsrucioprod
        mountPath: /opt/rucio/etc/
        subPaths:
          - gcs_rucio_prod.json
      - secretFullName: rse-accounts
        mountPath: /opt/rucio/etc/
        subPaths:
          - rse-accounts.cfg
      - secretFullName: mail-templates
        mountPath: /opt/rucio/etc/mail_templates/
        subPaths:
          - rule_ok_notification.tmpl
          - rule_approval_request.tmpl
          - rule_approved_admin.tmpl
          - rule_approved_user.tmpl
          - rule_denied_admin.tmpl
          - rule_denied_user.tmpl
      - secretFullName: list-replicas-seal-patch
        mountPath: /patch/list_replicas_seal.patch
        subPath: list_replicas_seal.patch
      - secretFullName: credential-gcs-patch
        mountPath: /patch/credential_gcs.patch
        subPath: credential_gcs.patch

    httpd_config:
      mpm_mode: "event"
      timeout: "300"
      enable_status: "True"
      keep_alive: "On"
      keep_alive_timeout: "5"
      max_keep_alive_requests: "128"
      server_limit: "10"
      start_servers: "4"
      thread_limit: "128"
      threads_per_child: "128"
      min_spare_threads: "256"
      max_spare_threads: "512"
      max_request_workers: "1280"
      max_connections_per_child: "2048"

    wsgi:
      daemonProcesses: "4"
      daemonThreads: "8"

    serverType: flask

    serverResources:
      limits:
        cpu: "1000m"
        memory: "2000Mi"
      requests:
        cpu: "700m"
        memory: "2000Mi"

    ingress:
      enabled: false

    ftsRenewal:
      enabled: 0  # will use the one from daemons helmrelease

    config:
      # The configurations shared by all servers must be set in ./serverprod_common_rucio.cfg. Setting the same value here will override it
      common:
        logformat: "%%(asctime)s\t%%(name)s\t%%(process)d\t%%(processName)d\t%%(thread)d\t%%(threadName)d\t%%(levelname)s\t%%(source.ip)s\t%%(network.forwarded_ip)s\t%%(client.account.name)s\t%%(user_agent.original)\t%%(http.request.method)s\t%%(url.full)s\t%%(message)s"

      api:
        endpoints: "accountlimits, accounts, archives, config, credentials, dids, export, heartbeats, identities, import, lifetime_exceptions, locks, meta, ping, redirect, replicas, requests, rses, rules, scopes, subscriptions, traces, nongrid_traces"

      trace:
        tracedir: "/var/log/rucio/trace"
        topic: "/topic/rucio.tracer"
        brokers: "atlas-mb.cern.ch"
        port: "61013"
        backupCount: "1"
        maxbytes: "500000000"

      nongrid_trace:
        tracedir: "/var/log/rucio/nongrid_trace"
        topic: "/topic/rucio.nongrid_tracer"
        brokers: "atlas-mb.cern.ch"
        port: "61013"
        backupCount: "1"
        maxbytes: "500000000"
