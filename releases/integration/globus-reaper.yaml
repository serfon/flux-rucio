---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: globus-reaper
  namespace: rucio
  annotations:
    flux.weave.works/automated: "false"
spec:
  releaseName: globus-reaper
  interval: 5m
  install:
    disableWait: true
  chart:
    spec:
      sourceRef:
        kind: HelmRepository
        name: rucio-charts
      chart: rucio-daemons
      version: 32.0.0
  valuesFrom:
  - kind: Secret
    name: db-secret-daemons
    valuesKey: values.yaml
  values:
    reaperCount: 1

    useDeprecatedImplicitSecrets: false

    image:
      repository: registry.cern.ch/docker.io/rucio/rucio-daemons
      tag: release-32.2.0
      pullPolicy: IfNotPresent

    automaticRestart:
      enabled: 1
      image:
        repository: bitnami/kubectl
        tag: 1.18
        pullPolicy: IfNotPresent
      schedule: "15 1 * * *"

    monitoring:
      enabled: true
      exporterPort: 8080
      targetPort: 8080
      interval: 30s
      telemetryPath: /metrics
      namespace: monitoring
      labels:
        release: prometheus-operator
        prometheus: cluster-monitor
      serviceMonitor:
        relabelings:
          - targetLabel: metric_source
            replacement: rucio-k8s

    secretMounts:
      - secretFullName: rucio-x509up
        mountPath: /opt/proxy
      - secretFullName: cern-ca-bundle
        mountPath: /opt/certs/
      - secretFullName: gcsrucio
        mountPath: /opt/rucio/etc/gcs_rucio.json
        subPath: gcs_rucio.json
      - secretFullName: globus-refresh-token
        mountPath: /opt/rucio/reaper/etc/globus-config.yml
        subPath: globus-config.yml

    reaper:
      includeRses: "NERSC_DATADISK"
      threads: 5
      chunkSize: 1000
      resources:
        limits:
          memory: "1500Mi"
          cpu: "1200m"
        requests:
          memory: "1500Mi"
          cpu: "800m"
      extraHostPathMounts:
        - hostPath: /etc/grid-security/certificates/
          mountPath: /etc/grid-security/certificates/
          readOnly: true
          type: DirectoryOrCreate
      additionalEnvs:
        - name: X509_USER_PROXY
          value: "/opt/proxy/x509up"

    ftsRenewal:
      enabled: 1
      image:
        repository: rucio/fts-cron
        tag: 1.31.1-java
        pullPolicy: Always
      script: "atlas"
      vos:
        - vo: "atlas"
          voms: "atlas:/atlas/Role=production"
      servers: "https://lcgfts3.gridpp.rl.ac.uk:8446,https://fts3-pilot.cern.ch:8446,https://fts.usatlas.bnl.gov:8446,https://fts3-test.gridpp.rl.ac.uk:8446,https://fts3-atlas.cern.ch:8446,https://fts3-devel.cern.ch:8446"
      secretMounts:
        - secretFullName: longproxy
          mountPath: /opt/rucio/certs/
      additionalEnvs:
        - name: RUCIO_FTS_SECRETS
          value: rucio-x509up
        - name: RUCIO_LONG_PROXY
          value: "ddmadmin_latest_x509up.rfc.proxy"
      hostPathMounts: []

    config:
      cache:
        url: memcached.rucio.svc.cluster.local:11211

      client:
        request_retries: "3"
        auth_host: "https://rucio-auth-prod.cern.ch:443"
        client_x509_proxy: "/opt/proxy/x509up"
        account: "ddmadmin"
        rucio_host: "https://rucio-lb-prod.cern.ch:443"
        auth_type: "x509_proxy"
        ca_cert: "/opt/certs/CERN-bundle.pem"

      common:
        loglevel: "INFO"
        logjson: "True"
        logformat: "%%(asctime)s\t%%(name)s\t%%(process)d\t%%(processName)d\t%%(thread)d\t%%(threadName)d\t%%(levelname)s\t%%(message)s"

      core:
        use_temp_tables: "True"
        default_mail_from: "atlas-adc-ddm-support@cern.ch"

      monitor:
        carbon_server: "rucio-graphite-prod.cern.ch"
        carbon_port: "8125"
        user_scope: "rucio-k8s-prod"
        enable_metrics: "True"

      policy:
        permission: "atlas"
        schema: "atlas"

      credentials:
        gcs: "/opt/rucio/etc/gcs_rucio.json"

      conveyor:
        globus_auth_app: "bnl"
