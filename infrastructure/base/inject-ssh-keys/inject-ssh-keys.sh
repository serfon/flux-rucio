#!/bin/bash

#set -x

function inject_ssh_key () {
    _authorized_keys="/hostfs/root/.ssh/authorized_keys"
    _public_key=$1
    if ! grep "${_public_key}" ${_authorized_keys} ; then
        echo "Will add ssh key: ${_public_key} to ${_authorized_keys}"
        echo "${_public_key}" >> ${_authorized_keys}
    else
       echo "ssh key already exists in ${_authorized_keys}"
    fi
    echo "sleeping for ever"
}

function remove_ssh_key () {
    _authorized_keys="/hostfs/root/.ssh/authorized_keys"
    _public_key=$1
    if ! grep "${_public_key}" ${_authorized_keys} ; then
        echo "ssh key: ${_public_key} not found in ${_authorized_keys}"
    else
       cat ${_authorized_keys} | grep -v "${_public_key}" > ${_authorized_keys}
       echo "ssh key ${_public_key} removed from ${_authorized_keys}"
    fi
    echo "sleeping for ever"
}

while read -r line
do
    inject_ssh_key "$line"
done < <(cat /opt/inject-ssh-keys/ssh-keys-to-inject.txt | grep -v ADD_KEYS_BELOW)

while read -r line
do
    remove_ssh_key "$line"
done < <(cat /opt/inject-ssh-keys/ssh-keys-to-remove.txt| grep -v ADD_KEYS_BELOW)


set +x
while true ; do
    #run
    sleep 60s
    echo "Restart me to re-run"
done

# end
