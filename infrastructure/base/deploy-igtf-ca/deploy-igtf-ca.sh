#!/bin/bash

set -e
set -x

cat <<EOF > /etc/yum.repos.d/ca.repo
[carepo]
name=IGTF CA Repository
baseurl=http://linuxsoft.cern.ch/mirror/repository.egi.eu/sw/production/cas/1/current/
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/GPG-KEY-EUGridPMA-RPM-3
EOF

dnf install -y epel-release.noarch
dnf --disablerepo="*" --enablerepo="carepo" -y install 'ca*'

dnf install -y \
    fetch-crl \
    rsync

/bin/cp -rf /tmp/fetch-crl.* /etc/
update-crypto-policies --set DEFAULT:SHA1

DST_PATH=/hostfs/etc/grid-security/certificates
mkdir -p "$DST_PATH"

while true ; do
  dnf update -y
  fetch-crl -v
  rsync --delete -av /etc/grid-security/certificates/ "$DST_PATH"

  MINWAIT=700
  MAXWAIT=740
  DURATION=$((MINWAIT+RANDOM % (MAXWAIT-MINWAIT)))m

  echo "Sleeping $DURATION"
  sleep $DURATION
done
